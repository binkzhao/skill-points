# 常见问题

***0. Docker 与虚拟机***

虚拟机：

我们传统的虚拟机需要模拟整台机器包括硬件，每台虚拟机都需要有自己的操作系统，虚拟机一旦被开启，预分配给他的资源将全部被占用，每一个虚拟机包括应用，必要的二进制和库，以及一个完整的用户操作系统。

Docker：

容器技术是和我们的宿主机共享硬件资源及操作系统可以实现资源的动态分配。

容器包含应用和其所有的依赖包，但是与其他容器共享内核。容器在宿主机操作系统中，在用户空间以分离的进程运行。

虚拟机和容器都是在硬件和操作系统以上的，虚拟机有 Hypervisor 层，Hypervisor 是整个虚拟机的核心所在。它为虚拟机提供了虚拟的运行平台，管理虚拟机的操作系统运行。每个虚拟机都有自己的系统和系统库以及应用。

容器没有 Hypervisor 这一层，并且每个容器是和宿主机共享硬件资源及操作系统，那么由 Hypervisor 带来性能的损耗，在 linux 容器这边是不存在的。

但是虚拟机技术也有其优势，能为应用提供一个更加隔离的环境，不会因为应用程序的漏洞给宿主机造成任何威胁。同时还支持跨操作系统的虚拟化，例如你可以在 linux 操作系统下运行 windows 虚拟机。

从虚拟化层面来看，传统虚拟化技术是对硬件资源的虚拟，容器技术则是对进程的虚拟，从而可提供更轻量级的虚拟化，实现进程和资源的隔离。

从架构来看，Docker 比虚拟化少了两层，取消了 hypervisor 层和 GuestOS 层，使用 Docker Engine 进行调度和隔离，所有应用共用主机操作系统，因此在体量上，Docker 较虚拟机更轻量级，在性能上优于虚拟化，接近裸机性能。

**1.Docker常用命令?**

- docker pull 拉取或者更新指定镜像
- docker push 将镜像推送至[远程仓库](http://www.bjpowernode.com/tutorial_git/1825.html)
- docker rm 删除容器
- docker rmi 删除镜像
- docker images 列出所有镜像
- docker ps 列出所有容器

**2.docker是怎么工作的?**

实际上docker使用了常见的CS架构，也就是client-server模式，docker client负责处理用户输入的各种命令，比如docker build、docker run，真正工作的其实是server，也就是docker demon，值得注意的是，docker client和docker demon可以运行在同一台机器上。

Docker是一个Client-Server结构的系统，Docker守护进程运行在主机上， 然后通过Socket连接从客户端访问，守护进程从客户端接受命令并管理运行在主机上的容器。守护进程和客户端可以运行在同一台机器上。

**3.docker容器之间怎么隔离?**

Linux中的PID、IPC、网络等资源是全局的，而NameSpace机制是一种资源隔离方案，在该机制下这些资源就不再是全局的了，而是属于某个特定的NameSpace，各个NameSpace下的资源互不干扰。

虽然有了NameSpace技术可以实现资源隔离，但进程还是可以不受控的访问系统资源，比如CPU、内存、磁盘、网络等，为了控制容器中进程对资源的访问，Docker采用control groups技术(也就是cgroup)，有了cgroup就可以控制容器中进程对系统资源的消耗了，比如你可以限制某个容器使用内存的上限、可以在哪些CPU上运行等等。

有了这两项技术，容器看起来就真的像是独立的操作系统了。

**4.容器与主机之间的数据拷贝命令?**

Docker cp命令用于穷奇与主机之间的数据拷贝

主机到哦容器：docker cp /www 96f7f14e99ab:/www/

容器到主机：docker cp 96f7f14e99ab:/www /tmp

**5.如何在生产中监控docker?**

Docker提供docker:stats和docker事件等工具来监控生产中的docker。我们可以使用这些命令获取重要统计数据的报告。

Docker统计数据：当我们使用容器ID调用docker stats时，我们获得容器的CPU，内存使用情况等。它类似于Linux中的top命令。

Docker事件：docker事件是一个命令，用于查看docker守护程序中正在进行的活动流。一些常见的docker事件是：attach，commit，die，detach，rename，destroy等。我们还可以使用各种选项来限制或过滤我们感性其的事件。

**6.DockerFile中的命令COPY和ADD命令有什么区别?**

COPY和ADD的区别时COPY的SRC只能是本地文件，其他用法一致。

**7.一个完整的Docker由哪些部分组成?**

- DockerClient客户端
- Docker Daemon守护进程
- Docker Image镜像
- DockerContainer容器 

**8.进入容器的方法有哪些?**

- 使用 docker attach 命令
- 使用 exec 命令，例如docker exec -i -t 784fd3b294d7 /bin/bash

**9.什么是联合文件系统(UnionFS)?**

docker的镜像实际上由一层一层的文件系统组成，这种层级的文件系统就是UnionFS。UnionFS是一种分层、轻量级并且高性能的文件系统。联合加载会把各层文件系统叠加起来，这样最终的文件系统会包含所有底层的文件和目录。