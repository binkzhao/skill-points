## 1. 堆排序

```go
// 排序
func HeapSort(a []int) {
	for i := len(a) / 2; i >= 0; i-- {
		HeapAdjust(a, i, len(a)-1)
	}
	for i := len(a) - 1; i > 0; i-- {
		a[0], a[i] = a[i], a[0]
		HeapAdjust(a, 0, i-1)
	}
}

// 建堆
func HeapAdjust(a []int, s, m int) {
	temp := a[s]
	for i := 2 * s; i <= m; i *= 2 {
		if i < m && a[i] < a[i+1] {
			i++
		}
		if temp >= a[i] {
			break
		}
		a[s] = a[i]
		s = i
	}
	a[s] = temp
}
```



## 2. 二叉树层次遍历

```go
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func levelOrder(root *TreeNode) [][]int {
    var result [][]int
	if root == nil {
		return result
	}

	q := []*TreeNode{root}
	for i := 0; len(q) > 0; i++ {
		result = append(result, []int{})
		var p []*TreeNode
		for j := 0; j < len(q); j++ {
			node := q[j]
			result[i] = append(result[i], node.Val)
			if node.Left != nil {
				p = append(p, node.Left)
			}
      if node.Right != nil {
				p = append(p, node.Right)
			}
		}
		q = p
	}

	return result
}
```



## 3. 二叉树前、中、后序遍历

### 1. 递归遍历

```go
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func inorderTraversal(root *TreeNode) []int {
	result := []int{}
	tra(root, &result)
	return result
}

func tra(node *TreeNode, res *[]int)  {
	if node == nil {
		return
	}
  // *res = append(*res,node.Val) // 前序
	tra(node.Left, res)
	*res = append(*res,node.Val) // 中序
	tra(node.Right, res)
  // *res = append(*res,node.Val) // 后序
}
```



### 2. 非递归遍历

```go
// 前序、中序遍历
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func inorderTraversal(root *TreeNode) []int {
	result := []int{}
  if root == nil {
    return result
  }
	nodes := []*TreeNode{}
	node := root
	for node != nil || len(nodes) > 0 {
		if node != nil {
      // result = append(result, node.Val) // 前序遍历
      nodes = append(nodes, node)
			node = node.Left
		} else {
			node = nodes[len(nodes)-1]
			nodes = nodes[:len(nodes)-1]
			result = append(result, node.Val) // 中序遍历
			node = node.Right
		}
	}
	return result
}

// 后序遍历
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func postorderTraversal(root *TreeNode) []int {
    var res []int
	var stack []*TreeNode
	var prev *TreeNode
	for root != nil || len(stack) > 0 {
		for root != nil {
			stack = append(stack, root)
			root = root.Left
		}
		root = stack[len(stack)-1]
		stack = stack[:len(stack)-1]
		if root.Right == nil || root.Right == prev {
			res = append(res, root.Val)
			prev = root
			root = nil
		} else {
			stack = append(stack, root)
			root = root.Right
		}
	}
	return res
}
```



## 4. 从前序与中序遍历序列构造二叉树

***思路***

对于任意一颗树而言，前序遍历的形式总是


[ 根节点, [左子树的前序遍历结果], [右子树的前序遍历结果] ]
即根节点总是前序遍历中的第一个节点。而中序遍历的形式总是


[ [左子树的中序遍历结果], 根节点, [右子树的中序遍历结果] ]
只要我们在中序遍历中定位到根节点，那么我们就可以分别知道左子树和右子树中的节点数目。由于同一颗子树的前序遍历和中序遍历的长度显然是相同的，因此我们就可以对应到前序遍历的结果中，对上述形式中的所有左右括号进行定位。

这样以来，我们就知道了左子树的前序遍历和中序遍历结果，以及右子树的前序遍历和中序遍历结果，我们就可以递归地对构造出左子树和右子树，再将这两颗子树接到根节点的左右位置。

```go
func buildTree(preorder []int, inorder []int) *TreeNode {
	if len(preorder) == 0 {
		return nil
	}

	root := &TreeNode{Val: preorder[0]}

	i := 0
	for ; i < len(inorder); i++ {
		if inorder[i] == preorder[0] {
			break
		}
	}
    
	root.Left = buildTree(preorder[1:len(inorder[:i])+1], inorder[:i])
	root.Right = buildTree(preorder[len(inorder[:i])+1:], inorder[i+1:])

	return root
}
```



## 5. 排序

```go
/**
* 使用golang实现常见排序算法，以及使用sort包来实现这些算法：统一都是升序排序
 */

// 冒泡排序
func BulleSort(items []int) {
	for i := 0; i < len(items); i++ {
		for j := 0; j < len(items)-i-1; j++ {
			if items[j+1] < items[j] {
				swap(&items[j], &items[j+1])
			}
		}
	}
}

// 冒泡排序 使用sort包
func BubbleSortUsingSortPackage(data sort.Interface) {
	len := data.Len()
	for i := 0; i < len-1; i++ {
		for j := 0; j < len-1-i; j++ {
			if data.Less(j+1, j) {
				data.Swap(j+1, j)
			}
		}
	}
}

// 插入排序
func InsertSort(items []int) {
	for i := 1; i < len(items); i++ {
		for j := i; j > 0; j-- {
			if items[j-1] <= items[j] {
				// 已经是有序，不需要再继续比较
				break
			}

			swap(&items[j-1], &items[j])
		}
	}
}

// 插入排序 使用sort包
func InsertSortUsingSortPackage(data sort.Interface) {
	r := data.Len() - 1
	for i := 1; i <= r; i++ {
		for j := i; j > 0 && data.Less(j, j-1); j-- {
			data.Swap(j, j-1)
		}
	}
}

// 简单选择排序
func SelectSort(items []int) {
	for i := 0; i < len(items)-1; i++ {
		var minIdx = i
		for j := i + 1; j < len(items); j++ {
			if items[j] < items[minIdx] {
				minIdx = j
			}
		}
		swap(&items[i], &items[minIdx])
	}
}

// 简单选择排序 使用sort包
func SelectSortUsingSortPackage(data sort.Interface) {
	r := data.Len() - 1
	for i := 0; i < r; i++ {
		min := i
		for j := i + 1; j <= r; j++ {
			if data.Less(j, min) {
				min = j
			}
		}
		data.Swap(i, min)
	}
}

// 快速排序
func QuickSort(src []int, first, last int) {
	flag := first
	left := first
	right := last

	if first >= last {
		return
	}

	for first < last {
		for first < last {
			if src[last] >= src[flag] {
				last--
				continue
			}
			swap(&src[last], &src[flag])
			flag = last
			break
		}

		for first < last {
			if src[first] <= src[flag] {
				first++
				continue
			}
			swap(&src[first], &src[flag])
			flag = first
			break
		}
	}

	QuickSort(src, left, flag-1)
	QuickSort(src, flag+1, right)
}

// 归并排序
func MergeSort(items []int) []int {
	len := len(items)
	if len == 1 {
		return items
	}

	middle := len / 2
	left, right := items[:middle], items[middle:]

	return merge(MergeSort(left), MergeSort(right))
}

func merge(left, right []int) (result []int) {
	result = make([]int, len(right)+len(left))
	var i, j, p int
	for i < len(left) && j < len(right) {
		if left[i] <= right[j] {
			result[p] = left[i]
			i++
		} else {
			result[p] = right[j]
			j++
		}
		p++
	}

	for i < len(left) {
		result[p] = left[i]
		p++
		i++
	}

	for j < len(right) {
		result[p] = right[j]
		p++
		j++
	}
	return
}

// 交换两个元素的值
func swap(a, b *int) {
	*a, *b = *b, *a
}
```



## 6.LRU缓存机制

LRU 是 Least Recently Used 的缩写，译为最近最少使用。它的理论基础为“**最近使用的数据会在未来一段时期内仍然被使用，已经很久没有使用的数据大概率在未来很长一段时间仍然不会被使用**”由于该思想非常契合业务场景 ，并且可以解决很多实际开发中的问题，所以我们经常通过 LRU 的思想来作缓存，一般也将其称为**LRU缓存机制**。因为恰好 leetcode 上有这道题，所以我干脆把题目贴这里。但是对于 LRU 而言，希望大家不要局限于本题（大家不用担心学不会，我希望能做一个全网最简单的版本，希望可以坚持看下去！）下面，我们一起学习一下。

```go
// 实例
LRUCache cache = new LRUCache( 2 /* 缓存容量 */ );
cache.put(1, 1);
cache.put(2, 2);
cache.get(1);       // 返回  1
cache.put(3, 3);    // 该操作会使得密钥 2 作废
cache.get(2);       // 返回 -1 (未找到)
cache.put(4, 4);    // 该操作会使得密钥 1 作废
cache.get(1);       // 返回 -1 (未找到)
cache.get(3);       // 返回  3
cache.get(4);       // 返回  4

// 简易版代码实现
type LinkNode struct {
	key, val  int
	pre, next *LinkNode
}

type LRUCache struct {
	m          map[int]*LinkNode
	cap        int
	head, tail *LinkNode
}

func Constructor(capacity int) LRUCache {
	head := &LinkNode{0, 0, nil, nil}
	tail := &LinkNode{0, 0, nil, nil}
	head.next = tail
	tail.pre = head
	return LRUCache{make(map[int]*LinkNode), capacity, head, tail}
}

func (this *LRUCache) Get(key int) int {
	cache := this.m
	if v, exist := cache[key]; exist {
		this.MoveToHead(v)
		return v.val
	} else {
		return -1
	}
}

func (this *LRUCache) RemoveNode(node *LinkNode) {
	node.pre.next = node.next
	node.next.pre = node.pre
}

func (this *LRUCache) AddNode(node *LinkNode) {
	head := this.head
	node.next = head.next
	head.next.pre = node
	node.pre = head
	head.next = node
}

func (this *LRUCache) MoveToHead(node *LinkNode) {
	this.RemoveNode(node)
	this.AddNode(node)
}

func (this *LRUCache) Put(key int, value int) {
	tail := this.tail
	cache := this.m
	if v, exist := cache[key]; exist {
		v.val = value
		this.MoveToHead(v)
	} else {
		v := &LinkNode{key, value, nil, nil}
		if len(cache) == this.cap {
			delete(cache, tail.pre.key)
			this.RemoveNode(tail.pre)
		}
		this.AddNode(v)
		cache[key] = v
	}
}
```



## 7. 三数之和

```go
func threeSum(nums []int, target int) [][]int {
   var result [][]int
   if len(nums) < 3 {
      return result
   }

   sort.Ints(nums)
   for i := 0; i < len(nums)-2; i++ {
      if nums[i] > target {
         break
      }
      lrVal := target - nums[i]
      l := i + 1
      r := len(nums) - 1
      if i == 0 || nums[i-1] != nums[i] {
         for l < r {
            if nums[l]+nums[r] == lrVal {
               result = append(result, []int{nums[i], nums[l], nums[r]})
               for l < r && nums[l] == nums[l+1] {
                  l++
               }
               for l < r && nums[r] == nums[r-1] {
                  r--
               }
               l++
               r--
            } else if nums[l]+nums[r] < lrVal {
               l++
            } else {
               r--
            }
         }
      }
   }

   return result
}
```
